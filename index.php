<?php
require_once 'StyledText.php';
require_once 'Image.php';

$heading = new StyledText('Nauja antraštė');
$heading->changeColor('green');
//$heading->color = 'green';
// (kai kintamieji klaseje yra private arba protected, tada sitaip pakeisti spalvos negalime, galime pakeisti tik su funkcija)
$heading->changeSize(32);
//$heading->size = 32;
//$heading->output();
echo $heading;

// $heading2 = new StyledText('Naujienos');
// $heading2->output();

// $heading3 = new StyledText();
// $heading3->output();


//NAMU DARBAS

$picture = new Image('https://think360studio.com/wp-content/uploads/2017/12/Learn-coding-online.jpeg');
$picture->setImageSize(800, 600);
//$picture->showImage();
echo $picture;