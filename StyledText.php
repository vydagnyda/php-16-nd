<?php
class StyledText
{
    protected $text;
    protected $color = 'red';
    protected $size = 14;

    public function __construct(string $text = 'My paragraph') 
    {
        $this->text = $text;
    }

    public function __toString(): string //"magic" funkcija
    {
        return '<div style="color: ' . $this->color . '; font-size: ' . $this->size . '"> ' . $this->text . '</div>';       
    } 

    // public function output(): void 
    // {
    //     echo '<div style="color: ' . $this->color . '; font-size: ' . $this->size . '"> ' . $this->text . '</div>';       
    // }

    public function changeColor(string $color): void
    {
        if ($color == 'white') {
            return;
        }
        $this->color = $color;
    }

    public function changeSize(int $size): void
    {
        if ($size  > 24 || $size < 8) {
            return;
        }
        $this->size = $size;
    }
}