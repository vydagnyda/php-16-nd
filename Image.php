<?php

class Image 
{
    protected $URL;
    protected $width;
    protected $height; 

    public function __construct(string $URL)
    {
        $this->URL = $URL;
    }

    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    // public function showImage(): void
    // {    
    //     echo '<img src="' . $this->URL . '" width="' . $this->width . '" height="' . $this->height . '">';
    // }

    public function __toString(): string //"magic" funkcija
    {
        return '<img src="' . $this->URL . '" width="' . $this->width . '" height="' . $this->height . '">';       
    } 

    public function setImageSize($width, $height): void
    {
        $this->width = $width;
        $this->height = $height;
    }
}
?>